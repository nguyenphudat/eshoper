<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       $data = [
       		'email' => 'customer@gmail.com',
       		'password' =>bcrypt('123456'), 
       		'name' => 'Customer', 
       		'phone' => '0982220985',
          'level' => '2',
       		'created_at' => new Datetime(), 
       ];
       DB::table('tbl_admin')->insert($data); 
    }
}
