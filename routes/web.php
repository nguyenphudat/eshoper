<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




//_________________________________________________Frontend

Route::get('/', 'IndexController@index');
Route::get('/trang-chu', 'IndexController@index');
Route::get('/home', 'IndexController@index')->name('home');
Route::post('/tim-kiem', 'IndexController@search');

//Danh mục sản phẩm trang chủ

Route::get('/danh-muc-san-pham/{category_product_id}', 'CategoryProduct@show_cate_home');


//Thương hiệu sản phẩm trang chủ
Route::get('/danh-muc-thuong-hieu/{brand_product_id}', 'BrandProduct@show_brand_home');

//Chi tiết sản phẩm trang chủ
Route::get('/chi-tiet-san-pham/{product_id}', 'ProductController@detail_product');










//End for Frontend






//_______________________________________________Backend
Route::get('/admin', 'AdminController@index');
Route::get('/dashboard', 'AdminController@checklogin');
Route::get('/admin_dashboard','AdminController@login');
Route::post('/admin_dashboard','AdminController@login');
Route::get('/logout','AdminController@logout');


//Category-Product
Route::get('/add-category-product','CategoryProduct@add_category_product');
Route::post('/save-category-product','CategoryProduct@save_category_product');

Route::get('/all-category-product','CategoryProduct@all_category_product');

Route::get('/unactive-category-product/{category_product_id}','CategoryProduct@unactive_category_product');
Route::get('/active-category-product/{category_product_id}','CategoryProduct@active_category_product');

Route::get('/edit-category-product/{category_product_id}','CategoryProduct@edit_category_product');
Route::get('/delete-category-product/{category_product_id}','CategoryProduct@delete_category_product');
Route::post('/update-category-product/{category_product_id}','CategoryProduct@update_category_product');


//Brand Product

Route::get('/add-brand-product','BrandProduct@add_brand_product');
Route::post('/save-brand-product','BrandProduct@save_brand_product');

Route::get('/all-brand-product','BrandProduct@all_brand_product');

Route::get('/unactive-brand-product/{brand_product_id}','BrandProduct@unactive_brand_product');
Route::get('/active-brand-product/{brand_product_id}','BrandProduct@active_brand_product');

Route::get('/edit-brand-product/{brand_product_id}','BrandProduct@edit_brand_product');
Route::get('/delete-brand-product/{brand_product_id}','BrandProduct@delete_brand_product');
Route::post('/update-brand-product/{brand_product_id}','BrandProduct@update_brand_product');

//Product
Route::get('/add-product','ProductController@add_product');
Route::post('/save-product','ProductController@save_product');

Route::get('/all-product','ProductController@all_product');

Route::get('/unactive-product/{product_id}','ProductController@unactive_product');
Route::get('/active-product/{product_id}','ProductController@active_product');


Route::get('/edit-product/{product_id}','ProductController@edit_product');
Route::get('/delete-product/{product_id}','ProductController@delete_product');
Route::post('/update-product/{product_id}','ProductController@update_product');


//Shoping Cart

Route::post('/save-cart','CartController@save_cart');
Route::get('/show-cart','CartController@show_cart');
Route::get('/delete-item-cart/{rowID}','CartController@cart_destroy');
Route::get('/update-qty','CartController@update_qty');
Route::post('/add-cart-ajax','CartController@add_cart_ajax');
Route::get('/gio-hang','CartController@gio_hang');
Route::get('/update-cart','CartController@update_cart');
Route::get('/delete-cart-id/{session_id}','CartController@delete_cart_id');
Route::get('/del-all-in-cart','CartController@del_all_in_cart');
Route::post('/update-cart-ajax','CartController@update_cart_ajax');

//Checkout
Route::get('/check-out','CheckoutController@login_check_out');
Route::get('/thanh-toan','CheckoutController@check_out');
Route::post('/save-info-order','CheckoutController@save_info_order');
Route::get('/check-out','CheckoutController@login_check_out');
Route::get('/payment','CheckoutController@payment');


//Customer Login

Route::get('/customer-login','CheckoutController@customer_login');
Route::post('/customer-login','CheckoutController@customer_login')->name('user.login');
Route::get('/customer-logout','CheckoutController@customer_logout');
Route::post('/register-customer','CheckoutController@register_customer')->name('user.register');


//Coupon

Route::post('/check-coupon','CartController@check_coupon');
Route::get('/insert-coupon','CouponController@insert_coupon');
Route::post('/add-coupon','CouponController@add_coupon');
Route::get('/list-coupon','CouponController@list_coupon');
Route::get('/delete-coupon','CouponController@delete_coupon');
Route::get('/del-coupon','CouponController@del_coupon');

 Route::group(['prefix' => 'laravel-filemanager'], function () {
     \UniSharp\LaravelFilemanager\Lfm::routes();
 });