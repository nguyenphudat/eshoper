<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
use App\Http\Controllers\AdminController;
use Illuminate\Support\Facades\Redirect;

class IndexController extends Controller
{
    
	public function index(Request $request){

		$meta_desc = "Chuyên bán hàng phụ kiện thời trang";
		$meta_keyword = "phu kien thoi trang, phụ kiện thời trang";
		$meta_title = "Phụ kiện thời trang Eshopper";
		$url_canonical = $request->url();
		$image_og = "{{url('public/frontend/images/home/logo.png')}}";

		
		$cate_product = DB::table('tbl_category_product')->where('category_status','1')->orderby('category_id','desc')->get();
    	$brand_product = DB::table('tbl_brand')->where('brand_status','1')->orderby('brand_id','desc')->get();
    	$all_product = DB::table('tbl_product')->where('product_status','1')->orderby('product_id','desc')->get();
    	return view('pages.home')->with('category',$cate_product)->with('brand',$brand_product)->with('all_product',$all_product)->with('url_canonical',$url_canonical)->with('meta_desc',$meta_desc)->with('meta_keyword',$meta_keyword)->with('meta_title',$meta_title)->with('image_og',$image_og);
    	// return view('pages.home')->with(compact('cate_product','brand_product','all_product'));
	}
	public function search(Request $request){

		$keyword = $request->keyword_submit;

		$meta_desc = "Chuyên bán hàng phụ kiện thời trang";
		$meta_keyword = "phu kien thoi trang, phụ kiện thời trang";
		$meta_title = "Phụ kiện thời trang Eshopper";
		$url_canonical = $request->url();
		$image_og = "{{url('public/frontend/images/home/logo.png')}}";

		
		$cate_product = DB::table('tbl_category_product')->where('category_status','1')->orderby('category_id','desc')->get();
    	$brand_product = DB::table('tbl_brand')->where('brand_status','1')->orderby('brand_id','desc')->get();
    	$search_product = DB::table('tbl_product')->where('product_name','like','%'.$keyword.'%')->get();
    	return view('pages.detail.search')->with('category',$cate_product)->with('brand',$brand_product)->with('url_canonical',$url_canonical)->with('meta_desc',$meta_desc)->with('meta_keyword',$meta_keyword)->with('meta_title',$meta_title)->with('image_og',$image_og)->with('search_product',$search_product);
	}

}
