<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
use App\Http\Controllers\AdminController;
use Illuminate\Support\Facades\Redirect;
use Auth;

class AdminController extends Controller
{

    public function index(){
    	return view('adminlogin');
    }

    public function checklogin(){
      if(Auth::check()){
        if(Auth::user()->level == 0){
        return view('admin.dashboard');
        }else{
          return Redirect::to('/admin');
        }
      }
      else{
        return Redirect::to('/admin');
      }
    }

    public function login(Request $request){
       if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
          return Redirect::to('/dashboard');
        }
        else{
          return Redirect::to('/admin');
          
        }
		
    }


    public function logout(){

    	Auth::logout();
      return Redirect::to('/admin');

    }


    
}
