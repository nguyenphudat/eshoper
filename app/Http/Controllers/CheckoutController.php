<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
use Auth;
use App\Http\Controllers\AdminController;
use Illuminate\Support\Facades\Redirect;
use Cart;
session_start();

class CheckoutController extends Controller
{

    //Thanh toán
    public function check_out(Request $request){
       $cate_product = DB::table('tbl_category_product')->where('category_status','1')->orderby('category_id','desc')->get();
      $brand_product = DB::table('tbl_brand')->where('brand_status','1')->orderby('brand_id','desc')->get();
      $meta_desc = "Chuyên bán hàng phụ kiện thời trang";
      $meta_keyword = "phu kien thoi trang, phụ kiện thời trang";
      $meta_title = "Phụ kiện thời trang Eshopper";
      $url_canonical = $request->url();
      $image_og = "{{url('public/frontend/images/home/logo.png')}}";

      return view('pages.checkout.checkout')->with('category',$cate_product)->with('brand',$brand_product)->with('url_canonical',$url_canonical)->with('meta_desc',$meta_desc)->with('meta_keyword',$meta_keyword)->with('meta_title',$meta_title)->with('image_og',$image_og);
    }



    //Đăng nhập customer
    public function login_check_out(Request $request){
      $cate_product = DB::table('tbl_category_product')->where('category_status','1')->orderby('category_id','desc')->get();
      $brand_product = DB::table('tbl_brand')->where('brand_status','1')->orderby('brand_id','desc')->get();
      $meta_desc = "Chuyên bán hàng phụ kiện thời trang";
      $meta_keyword = "phu kien thoi trang, phụ kiện thời trang";
      $meta_title = "Phụ kiện thời trang Eshopper";
      $url_canonical = $request->url();
      $image_og = "{{url('public/frontend/images/home/logo.png')}}";

      return view('pages.login.login_checkout')->with('category',$cate_product)->with('brand',$brand_product)->with('url_canonical',$url_canonical)->with('meta_desc',$meta_desc)->with('meta_keyword',$meta_keyword)->with('meta_title',$meta_title)->with('image_og',$image_og);
   }

   // public function checklogin(){
   //    if(Auth::check()){
   //      if(Auth::user()->level == 2){
   //      return redirect()->back();
   //      	dd('Đăng nhập thành công');
   //      }else{
   //        return Redirect::to('/admin');
   //      }
   //    }
   //    else{
   //      return Redirect::to('/admin');
   //    }
   //  }

    public function customer_login(Request $request){

        // if(Auth::check(){
          
        //   return Redirect::to('/thanh-toan');
        
        // }


       if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
          // return Redirect::to('/dashboard');
       		// dd('Đăng nhập thành công');
        if(Auth::user()){
          $customer_id = Auth::user()->id;
          return Redirect::to('/trang-chu');
        }
        }
        else{
          // return Redirect::to('/admin');
        	return redirect('/checkout');
          
        }
   }


   public function customer_logout(){
    Auth::logout();
    return Redirect::to('/trang-chu');
   }

   public function register_customer(Request $request){

      $data = array();
      $data['email'] = $request->customer_email;
      $data['password'] = bcrypt($request->customer_password);
      $data['name'] = $request->customer_name;
      $data['phone'] = $request->customer_phone;
      $data['level'] = $request->level;



      DB::table('tbl_admin')->insert($data);

      return redirect('/trang-chu');

   }

   public function save_info_order(Request $request){
      $data = array();
      $data['shipping_id'] = $request->shipping_id;
      $data['shipping_email'] = $request->shipping_email;
      $data['shipping_name'] = $request->shipping_name;
      $data['shipping_address'] = $request->shipping_address;
      $data['shipping_phone'] = $request->shipping_phone;
      $data['shipping_notes'] = $request->shipping_notes;

       $shipping_id =  DB::table('tbl_shipping')->insertGetId($data);
       Session::put('shipping_id',$shipping_id);

      return redirect('/payment');
   }

   public function payment(Request $request){
      $cate_product = DB::table('tbl_category_product')->where('category_status','1')->orderby('category_id','desc')->get();
      $brand_product = DB::table('tbl_brand')->where('brand_status','1')->orderby('brand_id','desc')->get();
      return view('pages.checkout.payment')->with('category',$cate_product)->with('brand',$brand_product);
   }
}
