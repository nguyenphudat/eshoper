<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
use App\Http\Controllers\AdminController;
use Illuminate\Support\Facades\Redirect;

class CategoryProduct extends Controller
{
    public function add_category_product(){
    	return view('admin.add_category_product');
    }

    public function all_category_product(){

    	$all_category_product = DB::table('tbl_category_product')->get();
    	return view('admin.all_category_product',['all_category_product' => $all_category_product]);
    }

    public function save_category_product(Request $request){

    	$data = array();
    	$data['category_name'] = $request->category_product_name;
    	$data['category_desc'] = $request->category_product_desc;
        $data['meta_keyword'] = $request->category_product_keyword;
    	$data['category_status'] = $request->category_product_status;

    	DB::table('tbl_category_product')->insert($data);

    	return view('admin.add_category_product');

    }

    public function edit_category_product($category_product_id){
    	$edit_category_product = DB::table('tbl_category_product')->where('category_id',$category_product_id)->get();
    	return view('admin.edit_category_product',['edit_category_product' => $edit_category_product]);
    }

    public function update_category_product(Request $request, $category_product_id){
    	$data = array();
    	$data['category_name'] = $request->category_product_name;
    	$data['category_desc'] = $request->category_product_desc;
        $data['meta_keyword'] = $request->category_product_keyword;
    	$data['category_status'] = $request->category_product_status;
    	DB::table('tbl_category_product')->where('category_id',$category_product_id)->update($data);
    	return Redirect::to('/all-category-product');
    }

    public function delete_category_product($category_product_id){
    	$edit_category_product = DB::table('tbl_category_product')->where('category_id',$category_product_id)->delete();
    	return Redirect::to('/all-category-product');
    }

    public function active_category_product($category_product_id){
         DB::table('tbl_category_product')->where('category_id',$category_product_id)->update(['category_status'=>0]);
        return Redirect::to('/all-category-product');
    }
    public function unactive_category_product($category_product_id){
        DB::table('tbl_category_product')->where('category_id',$category_product_id)->update(['category_status'=>1]);
        return Redirect::to('/all-category-product');
    }



    //________END ADMIN CATE


    //________FUNC FOR INDEX
    public function show_cate_home(Request $request,$category_product_id){


        
        $cate_product = DB::table('tbl_category_product')->where('category_status','1')->orderby('category_id','desc')->get();
        $brand_product = DB::table('tbl_brand')->where('brand_status','1')->orderby('brand_id','desc')->get();

        $category_by_id = DB::table('tbl_product')
        ->join('tbl_category_product','tbl_product.category_id','=','tbl_category_product.category_id')
        ->where('tbl_category_product.category_id',$category_product_id)->get();
        $cate_name =  DB::table('tbl_category_product')->where('category_id',$category_product_id)->get();

        foreach ($category_by_id as $key => $value) {
            $meta_desc = $value->category_desc;
            $meta_keyword = $value->meta_keyword;
            $meta_title = $value->category_name;
            $url_canonical = $request->url();
            $image_og = url('public/uploads/product/'.$value->product_image);
        
        
        return view('pages.category.show_category')->with('category',$cate_product)->with('brand',$brand_product)->with('category_by_id',$category_by_id)->with('cate_name',$cate_name)->with('meta_desc',$meta_desc)->with('meta_keyword',$meta_keyword)->with('meta_title',$meta_title)->with('url_canonical',$url_canonical)->with('image_og',$image_og);
    }
    }
}
