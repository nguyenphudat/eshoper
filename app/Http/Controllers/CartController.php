<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
use App\Http\Controllers\AdminController;
use Illuminate\Support\Facades\Redirect;
use Cart;
use App\Coupon;
session_start();


class CartController extends Controller
{
   public function save_cart(Request $request){
   	$proID = $request->productid_hidden;
   	$quanty = $request->quanty;
   	$product_info = DB::table('tbl_product')->where('product_id', $proID)->first();

   	$data['id'] = $product_info->product_id;
   	$data['qty'] = $quanty;
   	$data['name'] = $product_info->product_name;
   	$data['price'] = $product_info->product_price;	
   	$data['weight'] = '550';
   	$data['options']['image'] = $product_info->product_image;

   	Cart::add($data);
      // Cart::destroy();

   

   	return Redirect::to('/show-cart');
    

   }
   public function show_cart(){
   		$cate_product = DB::table('tbl_category_product')->where('category_status','1')->orderby('category_id','desc')->get();
    	$brand_product = DB::table('tbl_brand')->where('brand_status','1')->orderby('brand_id','desc')->get();
    	return view('pages.cart.show_cart')->with('category',$cate_product)->with('brand',$brand_product);
   }
   public function cart_destroy($rowID){
   			Cart::update($rowID,0);
   			return Redirect::to('/show-cart');
   		}

   public function update_qty(Request $request){
   		$rowId = $request->rowId_cart;
   		$qty = $request->quantity;
   		Cart::update($rowId,$qty);
   		return Redirect::to('/show-cart');
   }
  public function add_cart_ajax(Request $request){
      $data = $request->all();
      $session_id = substr(md5(microtime()), rand(0,26),5);
      $cart = Session::get('cart');
      if($cart == true ){
         $is_avaiable = 0;
         foreach ($cart as $key => $val) {
            if($val['product_id'] == $data['cart_product_id']){
               $is_avaiable++;
            }
         }
         
         if($is_avaiable == 0){
            $cart[] = array(
            'session_id' => $session_id,
            'product_name' => $data['cart_product_name'],
            'product_id' => $data['cart_product_id'],
            'product_image' => $data['cart_product_image'],
            'product_price' => $data['cart_product_price'],
            'product_qty' => $data['cart_product_qty'],
         );
             Session::put('cart',$cart);
         }
      } else{
         $cart[] = array(
           'session_id' => $session_id,
            'product_name' => $data['cart_product_name'],
            'product_id' => $data['cart_product_id'],
            'product_image' => $data['cart_product_image'],
            'product_price' => $data['cart_product_price'],
            'product_qty' => $data['cart_product_qty'],
         );
         Session::put('cart',$cart);
      }
      
      Session::save();
   }
   public function gio_hang(Request $request){
      $cart = Session::get('cart');

      $meta_desc = "Giỏ hàng của bạn"; 
      $meta_keyword = "Giỏ hàng Ajax";
      $meta_title = "Giỏ hàng của bạn";
      $url_canonical = $request->url();
      $image_og = "{{url('public/frontend/images/home/logo.png')";
      
      $cate_product = DB::table('tbl_category_product')->where('category_status','1')->orderby('category_id','desc')->get();
      $brand_product = DB::table('tbl_brand')->where('brand_status','1')->orderby('brand_id','desc')->get();
      return view('pages.cart.cart_ajax')->with('category',$cate_product)->with('brand',$brand_product)->with('meta_desc',$meta_desc)->with('meta_keyword',$meta_keyword)->with('meta_title',$meta_title)->with('url_canonical',$url_canonical)->with('image_og',$image_og);
   }

   public function update_cart(Request $request){
      $data = $request->all();
      $cart = Session::get('cart');
      Session::get('cart')['id']['qty'] = $request->get('id');
      if($cart == true){
         foreach ($data['cart_qty_'] as $key => $value) {
            foreach ($cart as $k => $val_cart) {
               if($val_cart['session_id'] == $key){
                  $cart[$k]['product_qty'] = $value;
               }
            }
         }
         Session::put('cart',$cart);
         return redirect()->back()->with('message','Cập nhật giỏ hàng thành công!');  
      }else{
         return redirect()->back()->with('message','Xóa sản phẩm thất bại!');
      }
   }

   public function delete_cart_id($session_id){
      $cart = Session::get('cart');
      if($cart == true){
         foreach ($cart as $key => $value) {
            if($value['session_id'] == $session_id){
               unset($cart[$key]);
            }
         }
         Session::put('cart',$cart);
         return redirect()->back()->with('message','Xóa sản phẩm thành công!');
         }else{
         return redirect()->back()->with('message','Xóa sản phẩm thất bại!');
      }
   }
   public function del_all_in_cart(){
       $cart = Session::get('cart');
       if($cart == true){
         Session::forget('cart');
         Session::forget('coupon');
         return redirect()->back()->with('message','Xóa hết giỏ hàng thành công!');
       } 
   }



   //Coupon--------------------


   public function check_coupon(Request $request){
      $data = $request->all();
      $coupon = Coupon::where('coupon_code',$data['coupon'])->first();
      if($coupon){
         $count_coupon = $coupon->count();
         if($count_coupon > 0){
            $coupon_session = Session::get('coupon');
            if($coupon_session==true){
               $is_avaiable = 0;
               if($is_avaiable == 0){
                  $cou[] = array(
                     'coupon_code' => $coupon->coupon_code,
                     'coupon_cond' => $coupon->coupon_cond,
                     'coupon_rage' => $coupon->coupon_rage,

                  );
                  Session::put('coupon',$cou);
                  }
               }else{
                  $cou[] = array(
                     'coupon_code' => $coupon->coupon_code,
                     'coupon_cond' => $coupon->coupon_cond,
                     'coupon_rage' => $coupon->coupon_rage,

                  );
                  Session::put('coupon',$cou);  
                  } 
               
            Session::save();
            return redirect()->back()->with('message','Thêm mã giảm giá thành công!');
         }
      }else{
         return redirect()->back()->with('error','Mã giảm giá không đúng!');
      }
   }

   public function update_cart_ajax(Request $request){
      $data = $request->all();
      $cart = Session::get('cart');
      Session::get('cart')['id']['qty'] = $request->get('id');
      if($cart == true){
         foreach ($data['cart_qty_'] as $key => $value) {
            foreach ($cart as $k => $val_cart) {
               if($val_cart['session_id'] == $key){
                  $cart[$k]['product_qty'] = $value;
               }
            }
         }
         Session::put('cart',$cart);
         return redirect()->back()->with('message','Cập nhật giỏ hàng thành công!');  
      }else{
         return redirect()->back()->with('message','Xóa sản phẩm thất bại!');
      }
   }
}
