<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Coupon;
use Session;
session_start();

class CouponController extends Controller
{
    public function insert_coupon(){
    	return view('admin.coupon.insert_coupon');
    }

    public function add_coupon(Request $request){
    	$data = $request->all();
    	$coupon = new Coupon;

    	$coupon->coupon_name = $data['coupon_name'];
    	$coupon->coupon_code = $data['coupon_code'];
    	$coupon->coupon_time = $data['coupon_time'];
    	$coupon->coupon_rage = $data['coupon_rage'];
    	$coupon->coupon_cond = $data['coupon_cond'];
    	$coupon->save();

    	Session::put('message','Thêm mã giảm giá thành công');
    	return redirect('/insert-coupon');
    }

    public function list_coupon(){
    	$coupon = Coupon::orderby('coupon_id','desc')->get();
    	return view('admin.coupon.list_coupon')->with(compact('coupon'));
    }

    public function delete_coupon($coupon_id){
    	$coupon = Coupon::find($coupon_id);
    	$coupon->delete();
    	Session::put('message','Xóa mã giảm giá thành công');
    	return redirect('/list-coupon');
    }
    public function del_coupon(){
       $coupon = Session::get('coupon');
       if($coupon == true){
         Session::forget('coupon');
         return redirect()->back()->with('message','Xóa mã khuyến mãi thành công!');
       } 
   }
}
