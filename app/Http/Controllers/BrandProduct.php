<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
use App\Http\Controllers\AdminController;
use Illuminate\Support\Facades\Redirect;

class BrandProduct extends Controller
{
    public function add_brand_product(){
    	return view('admin.add_brand_product');
    }

    public function all_brand_product(){

    	$all_brand_product = DB::table('tbl_brand')->get();
    	return view('admin.all_brand_product',['all_brand_product' => $all_brand_product]);
    }

    public function save_brand_product(Request $request){

    	$data = array();
    	$data['brand_name'] = $request->brand_product_name;
    	$data['brand_desc'] = $request->brand_product_desc;
    	$data['brand_status'] = $request->brand_product_status;

    	DB::table('tbl_brand')->insert($data);

    	return view('admin.add_brand_product');

    }

    public function edit_brand_product($brand_product_id){
    	$edit_brand_product = DB::table('tbl_brand')->where('brand_id',$brand_product_id)->get();
    	return view('admin.edit_brand_product',['edit_brand_product' => $edit_brand_product]);
    }

    public function update_brand_product(Request $request, $brand_product_id){
    	$data = array();
    	$data['brand_name'] = $request->brand_product_name;
    	$data['brand_desc'] = $request->brand_product_desc;
    	$data['brand_status'] = $request->brand_product_status;
    	DB::table('tbl_brand')->where('brand_id',$brand_product_id)->update($data);
    	return Redirect::to('/all-brand-product');
    }

    public function delete_brand_product($brand_product_id){
    	$edit_brand_product = DB::table('tbl_brand')->where('brand_id',$brand_product_id)->delete();
    	return Redirect::to('/all-brand-product');
    }
    public function active_brand_product($brand_product_id){
         DB::table('tbl_brand')->where('brand_id',$brand_product_id)->update(['brand_status'=>0]);
        return Redirect::to('/all-brand-product');
    }
    public function unactive_brand_product($brand_product_id){
        DB::table('tbl_brand')->where('brand_id',$brand_product_id)->update(['brand_status'=>1]);
        return Redirect::to('/all-brand-product');
    }

      //________END ADMIN BRAND


    //________FUNC FOR INDEX
    public function show_brand_home($brand_id){
        
        $cate_product = DB::table('tbl_category_product')->where('category_status','1')->orderby('category_id','desc')->get();
        $brand_product = DB::table('tbl_brand')->where('brand_status','1')->orderby('brand_id','desc')->get();

        $brand_by_id = DB::table('tbl_product')
        ->join('tbl_brand','tbl_product.brand_id','=','tbl_brand.brand_id')
        ->where('tbl_brand.brand_id',$brand_id)->get();
        $ten_thuong_hieu =  DB::table('tbl_brand')->where('brand_id',$brand_id)->get();
        
        return view('pages.brand.show_brand')->with('category',$cate_product)->with('brand',$brand_product)->with('brand_by_id',$brand_by_id)->with('ten_thuong_hieu',$ten_thuong_hieu);
    }
}
