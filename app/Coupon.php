<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Coupon;
class Coupon extends Model
{
	public $timestamps = false;
    protected $table = 'tbl_coupon';
    protected $fillable = [
        'coupon_name', 'coupon_code', 'coupon_time','coupon_cond', 'coupon_rage',
    ];
    protected $primaryKey = 'coupon_id';
}
