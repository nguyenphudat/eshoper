@extends('welcome')
@section('content')


<div class="features_items "><!--features_items-->
    <h2 class="title text-center">Sản phẩm mới nhất</h2>
    @foreach($all_product as $key => $product)
    
    <div class="col-sm-4">
        <div class="product-image-wrapper">
            
            <div class="single-products">
               <form>
                    <div class="productinfo text-center">

                    {{ csrf_field()}}
                    <input type="hidden" name="" value="{{$product->product_id}}" class="cart_product_id_{{$product->product_id}}">

                    <input type="hidden" name="" value="{{$product->product_name}}" class="cart_product_name_{{$product->product_id}}">

                    <input type="hidden" name="" value="{{$product->product_image}}" class="cart_product_image_{{$product->product_id}}">

                    <input type="hidden" name="" value="{{$product->product_price}}" class="cart_product_price_{{$product->product_id}}">

                    <input type="hidden" name="" value="1" class="cart_product_qty_{{$product->product_id}}">

                        <img src="{{URL::to('public/uploads/product/'.$product->product_image)}}" alt="" />
                        <h2>{{number_format($product->product_price).' '.'VNĐ' }}</h2>
                        <p>{{ $product->product_name }}</p>
                        <button type="button" class="btn btn-default add-to-cart" data-id="{{$product->product_id}}" name="add-to-cart">Thêm giỏ hàng</button>
                        
                    </div> 
                    <div class="product-overlay">
                        <div class="overlay-content">
                            <h2>{{number_format($product->product_price).' '.'VNĐ' }}</h2>
                            <p>{{ $product->product_name }}</p>
                            <button type="button" class="btn btn-default add-to-cart" data-id="{{$product->product_id}}" name="add-to-cart">Thêm giỏ hàng</button>
                        </div>
                    </div>
               </form> 
            </div>
           
            <div class="choose">
                <ul class="nav nav-pills nav-justified">
                    <li><a href="{{URL::to('/chi-tiet-san-pham/'.$product->product_id)}}"><i class="fa fa-plus-square"></i>Chi tiết</a></li>
                    <li><a href="#"><i class="fa fa-plus-square"></i>So sánh</a></li>
                </ul>
            </div>
        </div>
    </div>

    @endforeach
    
     
</div><!--features_items-->
 
<div class="category-tab"><!--category-tab-->
    <div class="col-sm-12">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#tshirt" data-toggle="tab">Sản phẩm được mua nhiều</a></li>
            
        </ul>
    </div>
    <div class="tab-content">
        <div class="tab-pane fade active in" id="tshirt" >
            <div class="col-sm-3">
                <div class="product-image-wrapper">
                    <div class="single-products">
                        <div class="productinfo text-center">
                            <img src="{{('public/frontend/images/home/gallery1.jpg')}}" alt="" />
                            <h2>$56</h2>
                            <p>Easy Polo Black Edition</p>
                            <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>

    </div>
</div><!--/category-tab-->




@endsection                