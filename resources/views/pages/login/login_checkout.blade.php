@extends('welcome')
@section('content')


<section id="form"><!--form-->
		<div class="container">
			<div class="row">
				<div class="col-sm-4 col-sm-offset-1">
					<div class="login-form"><!--login form-->
						<h2>Đăng nhập tài khoản</h2>


						<form action="{{route('user.login')}}" method="post">
							@csrf
							<input type="email" placeholder="Name" name="email" />
							<!-- <input type="email" placeholder="Email Address" /> -->
							<input type="password" placeholder="Password" name="password"/>
							<span>
								<input type="checkbox" class="checkbox"> 
								Keep me signed in
							</span>
							<button type="submit" class="btn btn-default">Login</button>
						</form>



					</div><!--/login form-->
				</div>
				<div class="col-sm-1">
					<h2 class="or">OR</h2>
				</div>
				<div class="col-sm-4">
					<div class="signup-form"><!--sign up form-->
						<h2>Đăng ký tài khoản mới</h2>


						<form action="{{url('/register-customer')}}" method="post">
							@csrf
							<input type="email" name="customer_email" placeholder="Nhập địa chỉ Email"/>
							<input type="password" name="customer_password" placeholder="Nhập vào mật khẩu"/>
							<input type="text" name="customer_name" placeholder="Nhập vào Họ và tên"/>
							<input type="text" name="customer_add" placeholder="Nhập vào địa chỉ"/>
							<input type="text" name="customer_phone" placeholder="Số điện thoại"/>
							<input type="hidden" name="level" value="2">
							<button type="submit" class="btn btn-default">Đăng ký</button>
						</form>



					</div><!--/sign up form-->
				</div>
			</div>
		</div>
	</section><!--/form-->


@endsection;