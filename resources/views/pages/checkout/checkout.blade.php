@extends('welcome')
@section('content')

<section id="cart_items">
		<!-- <div class="container"> -->
			<div class="breadcrumbs">
				<ol class="breadcrumb">
				  <li><a href="#">Home</a></li>
				  <li class="active">Check out</li>
				</ol>
			</div><!--/breadcrums-->

			
			<div class="register-req">
				<p>Hãy đăng nhập để thực hiện bước thanh toán đơn hàng. Nếu chưa có tài khoản vui lòng đăng ký mới</p>
			</div><!--/register-req-->

			<div class="shopper-informations">
				<div class="row">
					<div class="col-sm-10">
						<div class="shopper-info">
							<p>Thông tin đặt hàng</p>
							<form action="{{url('/save-info-order')}}" method="post">
								@csrf
								<input type="text" name="shipping_email" placeholder="Email">
								<input type="text" name="shipping_name" placeholder="Họ và tên nguời nhận">
								<input type="text" name="shipping_address" placeholder="Địa chỉ người nhận">
								<input type="text" name="shipping_phone" placeholder="Số điện thoại">
								
								<p>Ghi chú đặt hàng</p>
								<textarea name="shipping_notes"  placeholder="Ghi chú đơn đặt hàng của bạn" rows="5"></textarea>
							<!-- <label><input type="checkbox"> Shipping to bill address</label> -->
								<button type="submit" class="btn btn-primary">Gửi thông tin đặt hàng</button>
							</form>
							<!-- <a class="btn btn-primary" href="">Get Quotes</a> -->
							
						</div>
					</div>
					
			</div>
			<div class="clearfix" style="margin-bottom: 120px;">

			</div>
			
			<!-- <div class="payment-options">
					<span>
						<label><input type="checkbox"> Direct Bank Transfer</label>
					</span>
					<span>
						<label><input type="checkbox"> Check Payment</label>
					</span>
					<span>
						<label><input type="checkbox"> Paypal</label>
					</span>
				</div> -->
		
	</section>

@endsection