@extends('welcome')
@section('content')
<style type="text/css">
	 .animated {
    	-webkit-transition: height 0.2s;
    	-moz-transition: height 0.2s;
    	transition: height 0.2s;
	}

	.stars
	{
    	margin: 20px 0;
    	font-size: 24px;
    	color: #d17581;
	}
</style>
<div class="product-details"><!--product-details-->
						@foreach($detail_product as $key => $details)
						<div class="col-sm-5">
							<div class="view-product">
								<img src="{{url('public/uploads/product/'.$details->product_image)}}" alt="" />
								<h3>ZOOM</h3>
							</div>
							<div id="similar-product" class="carousel slide" data-ride="carousel">
								
								  <!-- Wrapper for slides -->
								    <div class="carousel-inner">
										<div class="item active">
										  <a href=""><img src="{{asset('public/frontend/images/product-details/similar1.jpg')}}" alt=""></a>
										  <a href=""><img src="{{asset('public/frontend/images/product-details/similar2.jpg')}}" alt=""></a>
										  <a href=""><img src="{{asset('public/frontend/images/product-details/similar3.jpg')}}" alt=""></a>
										</div>
										<div class="item">
										  <a href=""><img src="{{asset('public/frontend/images/product-details/similar1.jpg')}}" alt=""></a>
										  <a href=""><img src="{{asset('public/frontend/images/product-details/similar2.jpg')}}" alt=""></a>
										  <a href=""><img src="{{asset('public/frontend/images/product-details/similar3.jpg')}}" alt=""></a>
										</div>
										<div class="item">
										  <a href=""><img src="{{asset('public/frontend/images/product-details/similar1.jpg')}}" alt=""></a>
										  <a href=""><img src="{{asset('public/frontend/images/product-details/similar2.jpg')}}" alt=""></a>
										  <a href=""><img src="{{asset('public/frontend/images/product-details/similar3.jpg')}}" alt=""></a>
										</div>
										
									</div>

								  <!-- Controls -->
								  <a class="left item-control" href="#similar-product" data-slide="prev">
									<i class="fa fa-angle-left"></i>
								  </a>
								  <a class="right item-control" href="#similar-product" data-slide="next">
									<i class="fa fa-angle-right"></i>
								  </a>
							</div>

						</div>
						
						<div class="col-sm-7">
							<div class="product-information">
								<img src="{{asset('public/frontend/images/product-details/new.jpg')}}" class="newarrival" alt="" />
								<h2>{{$details->product_name}}</h2>
								<p>Mã ID: {{$details->product_id}}</p>
								<img src="{{asset('public/frontend/images/product-details/rating.png')}}" alt="" />
								
								<form action="{{url('/save-cart')}}" method="POST">
									@csrf
									<input type="hidden" value="{{$details->product_id}}" class="cart_product_id_{{$details->product_id}}">
                                    <input type="hidden" value="{{$details->product_name}}" class="cart_product_name_{{$details->product_id}}">
                                    <input type="hidden" value="{{$details->product_image}}" class="cart_product_image_{{$details->product_id}}">
                                    <input type="hidden" value="{{$details->product_price}}" class="cart_product_price_{{$details->product_id}}">
                                  
								<span>
									<span>{{number_format($details->product_price,0,',','.').'VNĐ'}}</span>
								
									<label>Số lượng:</label>
									<input name="qty" type="number" min="1" class="cart_product_qty_{{$details->product_id}}"  value="1" />
									<input name="productid_hidden" type="hidden"  value="{{$details->product_id}}" />
								</span>
								<!-- <input type="button" value="Thêm giỏ hàng" class="btn btn-primary btn-sm add-to-cart" data-id_product="{{$details->product_id}}" name="add-to-cart"> -->
								<button type="button" class="btn btn-primary btn-sm add-to-cart" data-id="{{$details->product_id}}" name="add-to-cart">Thêm giỏ hàng</button>
								</form>

								<p><b>Tình trạng:</b> Còn hàng</p>
								<p><b>Điều kiện:</b> Mơi 100%</p>
								<p><b>Thương hiệu:</b> {{$details->brand_name}}</p>
								<p><b>Danh mục:</b> {{$details->category_name}}</p>
								<div class="fb-like" data-href="{{$url_canonical}}" data-width="" data-layout="standard" data-action="like" data-size="small" data-share="true"></div>
							</div>
						</div>
						
					</div>
					<div class="category-tab shop-details-tab"><!--category-tab-->
						<div class="col-sm-12">
							<ul class="nav nav-tabs">
								<li class="active"><a href="#details" data-toggle="tab">Chi tiết</a></li>
								<li><a href="#companyprofile" data-toggle="tab">Mô tả sản phẩm</a></li>
								<li><a href="#reviews" data-toggle="tab">Bình luận</a></li>
								
							</ul>
						</div>
						<div class="tab-content">
							<div class="tab-pane fade active in" id="details" >
								<p>{!! $details->product_content !!} </p>
								
							</div>
							
							<div class="tab-pane fade" id="companyprofile" >
								<p>{!! $details->product_desc !!} </p>
								
							</div>
							
							
							
							<div class="tab-pane fade " id="reviews" >
								<div class="col-sm-12">
									<div class="fb-comments" data-href="{{$url_canonical}}" data-numposts="20" data-width=""></div>
								</div>
							</div>
							
								<div class="component_rating">
									<div class="component_rating_content" style="display: flex; align-items: center;">
										<div class="rating_item" style="width: 20%">
											<div class=""><span class="fa fa-star"></span><b>2.5</b></div>
										</div>
										<div class="list_rating" style="width: 60%; padding: 20px;">
											@for($i = 1 ; $i <= 5; $i++)
											<div class="item_rating" style="display: flex; align-items: center;">
												 <div style="width: 10%">
												 	{{ $i }}<span class="fa fa-star"></span>
												 </div>
												 <div style="width: 70%; margin:0 20px; margin-left: -10px">
												 	<span style="width: 100%; height: 6px; display: block;border: 1px solid #dedede"><b></b></span>
												 </div>
												 <div style="width: 20%">
												 	<a href="">290 đánh giá</a>
												 </div>
											</div>
											@endfor
										</div >
										<div style="width: 20%"><a href="">Gửi đánh giá</a></div>
									</div>	
								</div>
							
							
						</div>
								 
							</div>
							
						</div>

					</div>
					@endforeach
					<div class="recommended_items"><!--recommended_items-->
						<h2 class="title text-center">recommended items</h2>
						
						<div id="recommended-item-carousel" class="carousel slide" data-ride="carousel">
							<div class="carousel-inner">
								@foreach($related_product as $key => $related)
								<div class="item {{ $loop->first ?  'active' : ''}}">
									<div class="col-sm-4">
										<div class="product-image-wrapper">
											<div class="single-products">
												<div class="productinfo text-center">
													<img src="{{asset('public/uploads/product/'.$related->product_image)}}" alt="" />
													<h2>{{number_format($related->product_price).' '.'VNĐ' }}</h2>
													<p>{{$related->product_name}}</p>
													<button type="button" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</button>
												</div>
											</div>
										</div>
									</div>

								@endforeach
								</div>
								
							<!-- </div>
							 <a class="left recommended-item-control" href="#recommended-item-carousel" data-slide="prev">
								<i class="fa fa-angle-left"></i>
							  </a>
							  <a class="right recommended-item-control" href="#recommended-item-carousel" data-slide="next">
								<i class="fa fa-angle-right"></i>
							  </a>			
						</div> -->
					</div>
@endsection