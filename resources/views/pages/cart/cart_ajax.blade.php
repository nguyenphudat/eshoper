@extends('welcome')
@section('content')


<section id="cart_items">
		
			<div class="breadcrumbs">
				<ol class="breadcrumb">
				  <li><a href="{{URL('/trang-chu')}}">Home</a></li>
				  <li class="active">Shopping Cart</li>
				</ol>
			</div>
			<div class="table-responsive cart_info">
				
				@if(session()->has('message'))
                    <div class="alert alert-success">
                        {{ session()->get('message') }}
                    </div>
	              @elseif(session()->has('error'))
	                     <div class="alert alert-danger">
	                        {{ session()->get('error') }}
	                    </div>
	              @endif

				<form action="{{url('/update-cart')}}" method="">
					@csrf
				<table class="table table-condensed">
					<thead>
						<tr class="cart_menu">
							<td class="image">Sản phẩm</td>
							<td class="description">Tên sản phẩm</td>
							<td class="price">Giá</td>
							<td class="quantity">Số lượng</td>
							<td class="total">Tổng tiền</td>
							<td></td>
						</tr>
					</thead>
					<tbody>
						@if(Session::get('cart')==true)
							<?php 
								$total = 0;
							?>
						@foreach(Session::get('cart') as $cart)
							<?php 

								$subtotal = $cart['product_qty'] * $cart['product_price'];
								$total+=$subtotal ;
							?>
							<!-- <?php
								echo '<pre>' ;
								print_r($cart);
								echo '</pre>' ;
							 ?> -->
						<tr>
							<td class="cart_product">
								<a href=""><img style="max-height: 100px; max-width: 100px;" src="{{asset('public/uploads/product/'.$cart['product_image'])}}" alt=""></a>
							</td>
							<td class="cart_description">
								<h4><a href="">{{$cart['product_name']}} </a></h4>
								
							</td>
							<td class="cart_price">
								<p>{{number_format($cart['product_price'],0,',','.')}} đ</p>
							</td>
							<td class="cart_quantity">
								<div class="cart_quantity_button">

									<input class="cart_quantity_input" type="number" name="cart_qty_[{{$cart['session_id']}}]" value="{{$cart['product_qty']}}" min="1" size="15px" onchange="updateCart(this.value,'{{$cart['product_id']}}')">
								</div>
							</td>
							
							<td class="cart_total">
								<p class="cart_total_price">
										@php echo number_format($subtotal,0,',','.').' '."đ"; @endphp
										
								</p>
							</td>
							<td class="cart_delete">
								<a class="cart_quantity_delete" href="{{url('/delete-cart-id/'.$cart['session_id'])}}"><i class="fa fa-times"></i></a>
							</td>
						</tr>
						
							
					

						@endforeach
						<tr><td>
							<input class="btn btn-default check_out" type="submit" name="update-qty" value="Cập nhật">
						</td>
						<td>
							<a class="btn btn-default check_out" href="{{url('/del-all-in-cart')}}">Xóa tất cả</a>
						</td>
						<td>
							<a class="btn btn-default check_out" href="{{url('/del-coupon')}}">Xóa mã khuyến mãi</a>
						</td>
						<td></td>
						
						<td>
							<ul>
								<li>Tổng:<span>{{number_format($total,0,',','.') }}đ</span></li>
								@if(Session::get('coupon'))
								<li>
									@foreach(Session::get('coupon') as $key => $cou)
										@if($cou['coupon_cond'] == 1)
											Mã giảm: {{ $cou['coupon_rage'] }} %
											@php 
												$total_cou = ($total*$cou['coupon_rage'])/100;
												echo '<li>Số tiền giảm:'.number_format($total_cou,0,',','.').'đ</li>';
											@endphp
											<li>Tổng thanh toán:{{number_format($total-$total_cou,0,',','.')}}đ</li>
										@else
											Số tiền giảm: {{ number_format($cou['coupon_rage'],0,',','.') }} VNĐ
											<li>Tổng thanh toán:{{number_format($total-$cou['coupon_rage'],0,',','.')}}đ</li>
										@endif
									@endforeach
								</li>
								@endif
								<!-- <li>Thuế: <span> 0 VNĐ</span></li> -->
								<!-- <li>Phí vận chuyển:<span>Free</span></li>
								<li>Thành tiền:<span>{{number_format($total,0,',','.') }}đ</span></li> -->
							</ul>
						</td>
						
						

						</tr>
						
						
						@else
						 <tr><td colspan="5"><center>
						 	@php
						 	echo 'Làm ơn thêm sản phẩm vào giỏ hàng';
						 	@endphp
						 </center></td></tr>

						 @endif
					</tbody>
					
			
				
				</form>
				@if(Session::get('cart'))
				<tr><td>
				<form action="{{url('/check-coupon')}}" method="post">
					@csrf
					
						<input type="text" class="form-control" name="coupon" placeholder="Nhập mã giảm giá">
						<input type="submit" class="btn btn-default check-coupon" value="Tính mã giảm giá" name="check_coupon">
					
				</form>
				</td></tr>
				@endif
				</table>
			</div>
		


@endsection