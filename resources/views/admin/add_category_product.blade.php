@extends('admin_layout')
@section('admin_content')

<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                Thêm danh mục sản phẩm
            </header>
            <div class="panel-body">
                <div class="position-center">
                    <form role="form" action="{{url('/save-category-product')}}" method="post">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="exampleInputEmail1">Tên danh mục</label>
                            <input type="text" name="category_product_name" class="form-control"  id="exampleInputEmail1" placeholder="Nhập tên danh mục ">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Mô tả danh mục</label>
                            <textarea style="resize: none" rows="5" name="category_product_desc" class="form-control" id="exampleInputPassword1" placeholder="Mô tả danh mục"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Mô tả từ khóa</label>
                            <textarea style="resize: none" rows="5" name="category_product_keyword" class="form-control" id="exampleInputPassword1" placeholder="Mô tả danh mục"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Trạng thái</label>
                            <select name="category_product_status" class="form-control input-sm m-bot15">
                                <option value="0">Ẩn</option>
                                <option value="1">Hiển thị</option>      
                            </select>
                        </div>
                        <div class="form-group">
                           <div class="input-group">
                             <span class="input-group-btn">
                                <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary">
                                 <i class="fa fa-picture-o"></i> Choose
                                </a>
                             </span>
                                <input id="thumbnail" class="form-control" type="text" name="filepath">
                           </div>
                        <img id="holder" style="margin-top:15px;max-height:100px;">
                    </div> 
                 <button type="submit" name="add_category_product" class="btn btn-info">Thêm danh mục</button>
             </form>
         </div>

     </div>
 </section>

</div>
<script type="text/javascript">
    $('#lfm').filemanager('image');
</script>


@endsection