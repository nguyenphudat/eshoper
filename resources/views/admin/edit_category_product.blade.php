@extends('admin_layout')
@section('admin_content')

<div class="row">
            <div class="col-lg-12">
                    <section class="panel">
                        <header class="panel-heading">
                            Cập nhật danh mục sản phẩm
                        </header>
                        <div class="panel-body">
                            @foreach($edit_category_product as $edit_category_product)
                            <div class="position-center">
                                <form role="form" action="{{URL::to('/update-category-product/'.$edit_category_product->category_id)}}" method="post">
                                    {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Tên danh mục</label>
                                    <input type="text" value="{{ $edit_category_product->category_name }}" name="category_product_name" class="form-control"  id="exampleInputEmail1" placeholder="Nhập tên danh mục ">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Mô tả danh mục</label>
                                    <textarea style="resize: none" rows="5" name="category_product_desc" class="form-control" id="exampleInputPassword1">{{ $edit_category_product->category_desc}}</textarea>
                                </div>
                                 <div class="form-group">
                                    <label for="exampleInputPassword1">Mô tả từ khóa</label>
                                    <textarea style="resize: none" rows="5" name="category_product_keyword" class="form-control" id="exampleInputPassword1" placeholder="Mô tả danh mục">{{ $edit_category_product->meta_keyword}}</textarea>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Trạng thái</label>
                                    <select name="category_product_status" class="form-control input-sm m-bot15">
                                            <option value="0">Ẩn</option>
                                            <option value="1">Hiển thị</option>      
                                    </select>
                                </div>
                                
                                <button type="submit" name="edit_category_product" class="btn btn-info">Cập nhật</button>
                            </form>
                            </div>
                            @endforeach

                        </div>
                    </section>

            </div>

@endsection