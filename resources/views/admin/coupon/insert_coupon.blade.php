@extends('admin_layout')
@section('admin_content')

<div class="row">
            <div class="col-lg-12">
                    <section class="panel">
                        <header class="panel-heading">
                            Thêm mã giảm giá
                        </header>
                        <div class="panel-body">
                            <div class="position-center">
                                <?php 
                                    $message = Session::get('message');
                                    if($message){
                                        echo '<span class="text-danger">'.$message.'</span>';
                                        Session::put('message',null);
                                    }
                                ?>
                                <form role="form" action="{{URL::to('/add-coupon')}}" method="post">
                                    {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Tên mã giảm giá</label>
                                    <input type="text" name="coupon_name" class="form-control"  id="exampleInputEmail1" placeholder="Nhập tên danh mục ">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Mã giảm giá</label>
                                    <input type="text" name="coupon_code" class="form-control"  id="exampleInputEmail1" placeholder="Nhập tên danh mục ">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Số lượng mã</label>
                                    <input type="text" name="coupon_time" class="form-control"  id="exampleInputEmail1" placeholder="Nhập tên danh mục ">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Tính năng</label>
                                    <select name="coupon_cond" class="form-control input-sm m-bot15">
                                            <!-- <option value="0">------Chọn------</option> -->
                                            <option value="1">Giảm theo phần trăm</option>
                                            <option value="2">Giảm theo số tiền</option>      
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Nhập số % hoặc số tiền giảm</label>
                                    <input type="text" name="coupon_rage" class="form-control"  id="exampleInputEmail1" placeholder="Nhập tên danh mục ">
                                </div>
                                <button type="submit" name="add_coupon" class="btn btn-info">Thêm mã giảm</button>
                            </form>
                            </div>

                        </div>
                    </section>

            </div>

@endsection