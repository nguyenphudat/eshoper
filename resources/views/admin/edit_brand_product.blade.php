@extends('admin_layout')
@section('admin_content')

<div class="row">
            <div class="col-lg-12">
                    <section class="panel">
                        <header class="panel-heading">
                            Cập nhật thương hiệu
                        </header>
                        <div class="panel-body">
                            @foreach($edit_brand_product as $edit_brand_product)
                            <div class="position-center">
                                <form role="form" action="{{URL::to('/update-brand-product/'.$edit_brand_product->brand_id)}}" method="post">
                                    {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Tên danh mục</label>
                                    <input type="text" value="{{ $edit_brand_product->brand_name }}" name="brand_product_name" class="form-control"  id="exampleInputEmail1" placeholder="Nhập tên danh mục ">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Mô tả danh mục</label>
                                    <textarea style="resize: none" rows="5" name="brand_product_desc" class="form-control" id="exampleInputPassword1">{{ $edit_brand_product->brand_desc}}</textarea>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Trạng thái</label>
                                    <select name="brand_product_status" class="form-control input-sm m-bot15">
                                            <option value="0">Ẩn</option>
                                            <option value="1">Hiển thị</option>      
                                    </select>
                                </div>
                                
                                <button type="submit" name="edit_brand_product" class="btn btn-info">Cập nhật</button>
                            </form>
                            </div>
                            @endforeach

                        </div>
                    </section>

            </div>

@endsection